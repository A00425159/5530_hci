# HCI - assignment due date January 23rd (1 or 2 pages report)

# Carl Pooling

## problem identification

### problem/task domain

* number of cars in the road
* parking fees
* traffic
* more accidents (the more cars the higher probability of accidents)
* commuting to/from specific places (universities of Halifax)
* Public transit cannot fit schedule

### target user groups

* students and faculty staff from Halifax universities
* specifically students that have long commutes (over 1h by bus or over 30min by car)
* someone who wants reduce their oil cost
* cannot afford taxi and do not have own cars but need frequently

### motivation

* reduce car polution
* reduce cars in the road
* improve commute for students and faculty members that have long commutes
* create students/faculty members networking

### problems/issues to be addressed

* long commutes
* polution by fossil fuels
* traffic
..* and all that comes with it


### additional notes
students and faculties from same location (street) commute to Univ in their own cars.

### Existing App
www.hfxridematch.ca
[Ride Match](www.hfxridematch.ca)

---

# Yifan 

Living as foreign students must have many troubles in all aspects, besides the food, the living condition, traffic is another important part in basic necessities. Some of them need more than one hour bus ride everday for school.
However, in fact, traffic is not just foreign students' problem - some local students living far away, some university faculty afford flue costs by their own. 
By combining their different needs, carl pooling platform came into being, users can use this platform to find some one share their fule cost by give others a ride, or find a driver to save time if they are tired of taking a bus.
Besides, this platform can also be used to solve non-daily commutes. For instance, someone wants to go IKEA or COSTCO which is far away from downtown, this system will be stil useful.

---

# Wasic 

** (write here) **

---

# Naren
1.	Carpooling is also a more environmentally friendly and sustainable way to travel as sharing journeys reduces air pollution, carbon emissions, traffic congestion on the roads, and the need for parking spaces.
2.	Carpooling cuts down on the number of cars and vehicles on the road. Fewer cars means there is less carbon and other gasses and pollution getting into the air. This protects the environment by keeping the air, water, and land cleaner.
3.	According to numerous health reports and research, air pollution caused by auto emissions can significantly increase the likelihood of health issues such as asthma, allergies, lung cancer, COPD, and the like. Research data has also suggested that carpooling can be far less stressful than simply commuting on your own.
4.	Carpooling is also a wonderful way to meet interesting people, get to know the people you work with or go to school with, and to make new friends!
5.	World Health Organization has estimated that over one million deaths per year worldwide can be attributed to outdoor air pollution, which is to a large part caused by vehicular traffic

---
# Daniel

### problem/task domain

The number of vehicles on the roads nowadays are immense. Cars, and other vehicles, are responsible for a good amount of pollution. Air pollution has several impacts on our lives and on society as a whole.
According to numerous health reports and research, air pollution caused by auto emissions can significantly increase the likelihood of health issues such as asthma, allergies, lung cancer, COPD, and the like. Think of healthcare costs...
World Health Organization has estimated that over one million deaths per year worldwide can be attributed to outdoor air pollution, which is to a large part caused by vehicular traffic.
The number of cars on the roads have (almost) implicit costs to our lives. Parking fees, increase accident risk, health care, insurance, security, etc.
People have daily (or very frequent) commutes. In many cases these are not short commutes and in many many cases this commute is done using a car that has empty seats asking to be used.
In many cases people are "forced" to use their own cars because the public transportation system can not fit their needs.
Very often the commutes includes specific locations, like to company offices or universities, that aggregates a lot of people.

### target user groups

Our idea is targeted at this kind of locations and aggregations of people. Like large universities or companies where the user base is readily available.
Faculty members and students in a university can share car rides for commutes frequently. Specifically students or faculty members that have long commutes (over 1h by bus or over 30min by car).
Sharing rides have direct positive measurable effects. Drivers can reduce their fuel expenses and possibly parking expenses, riders will benefit from quicker and possibly more comfortable commutes.
On the long run and as a result of an aggregate of users sharing rides a number of other benefits will include: less air pollution, parking fees less expensive (by reduced demand), reduced health care costs, etc.
As a side benefit, if government can spend less on healthcare it can spend on other core areas for the society.

### motivation

The motivation for this project is to reduce air pollution by reducing the number of cars on the road. Also improve commutes for students and faculty staff. As a side effect, we expect a networking to be born from these rides.

### problems/issues to be addressed

    + long commutes
    + polution by fossil fuels
    + traffic
        - and all that comes with it

---
Consolidated:

Problem/Task Domain
The number of vehicles on the roads nowadays are immense. Cars, and other vehicles, are responsible for a good amount of pollution and large consumption of fossil fuels. Air pollution has several impacts on our lives and on society. People have daily (or very frequent) commutes. In many cases these are not short commutes and in many cases this commute is done using a car that has empty seats asking to be used. In many cases people are "forced" to use their own cars because the public transportation system can not fit their needs. Very often the commutes include specific locations, like to company offices or universities, that aggregates a lot of people. 

Target User groups
Our idea is targeted at this kind of locations and aggregations of people. Like large universities or companies where the user base is readily available. Faculty members and students in a university can share car rides for commutes frequently. Specifically, students or faculty members who have long commutes (over 1h by bus or over 30min by car). Sharing rides have direct positive measurable effects. Drivers can reduce their fuel expenses and possibly parking expenses, riders will benefit from quicker and possibly more comfortable commutes. 

Motivation
The motivation for this project is to reduce air pollution by reducing the number of cars on the road thereby reducing the consumption of fossil fuels. Also improve commutes for students and faculty staff. As a side effect, we expect a networking to be born from these rides.

Problem/Issues to be addressed
According to numerous health reports and research, air pollution caused by auto emissions can significantly increase the likelihood of health issues such as asthma, allergies, lung cancer, COPD, and the like. World Health Organization has estimated that over one million deaths per year worldwide can be attributed to outdoor air pollution, which is to a large part caused by vehicular traffic. The number of cars on the roads have (almost) implicit costs to our lives. Parking fees, increase accident risk, health care, insurance, security, etc. 
Our idea is to create an application for car pooling targeting commuters of University. The students and the faculties will be benefitted and it reduces the no of cars and consumption of fuels. On the long run and because of an aggregate of users sharing rides many other benefits will include: less air pollution, parking fees less expensive (by reduced demand), reduced health care costs, etc. As a side benefit, if government can spend less on healthcare it can spend on other core areas for the society.
Another aspect, Foreign students must have many troubles, besides food & living condition, traffic is another important part in daily routine. Specifically, students who have long commutes (over 1h by bus). Sharing rides have direct positive measurable effects. Besides, this platform can also be used to solve non-daily commutes. For instance, someone who wish to go to IKEA or COSTCO which is far away from downtown, it helps to a greater extent.

